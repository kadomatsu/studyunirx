
using System;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

/// <summary>
/// OnNextメッセージのサンプル
/// 時間切れを通知する
/// </summary>
namespace Samples.Section2.Observables
{

    public class MessageSample : MonoBehaviour
    {
        // 残り時間
        [SerializeField] private float m_countTimeSeconds = 5f;

        // 時間切れを通知するObservable
        public IObservable<Unit> OnTimeUpAsyncSubject => m_onTimeUpAsyncSubject;

        // AsyncSubject メッセージを一度だけ発行できるSubject
        private readonly AsyncSubject<Unit> m_onTimeUpAsyncSubject
            = new AsyncSubject<Unit>();

        private IDisposable m_disposable;


        // Start is called before the first frame update
        void Start()
        {
            // 指定時間したらメッセージを通知する
            m_disposable = Observable.Timer(TimeSpan.FromSeconds(m_countTimeSeconds))
                .Subscribe(_ =>
                    {
                        // Timerが発火したら
                        // Unit型のメッセージを発行する
                        m_onTimeUpAsyncSubject.OnNext(Unit.Default);
                        m_onTimeUpAsyncSubject.OnCompleted();

                        // 本当に動いているかわからなかったため、追加
                        Debug.Log("conpleted()");
                    }
                );
        }

        private void OnDestroy()
        {
            // Observableがまだ動いていたら止める
            m_disposable?.Dispose();
            // AsyncSubjectを破棄する
            m_onTimeUpAsyncSubject.Dispose();
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;

namespace Samples.Section2.Observables
{
    public class ReleaseWithDispose : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            var subject = new Subject<int>();

            // 同じSubjectを3回Subscribeする
            // subscribeの返り値のIDisposable.Dispose()でリソース開放する
            IDisposable disposableA
                = subject.Subscribe(x => Debug.Log("A:" + x));
            IDisposable disposableB
                = subject.Subscribe(x => Debug.Log("B:" + x));
            IDisposable disposableC
                = subject.Subscribe(x => Debug.Log("C:" + x));

            // 値の発行
            subject.OnNext(100);
            // AのみDispose
            disposableA.Dispose();
            Debug.Log("--- ---");
            // 値を再度発行
            subject.OnNext(200);

            // subject自体を開放
            subject.OnCompleted();
            subject.Dispose();
            
        }
    }
}

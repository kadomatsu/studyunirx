using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;


namespace Samples.Section2.Observables
{
    public class AddToSample : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            Observable.IntervalFrame(5)
                .Subscribe(_ => Debug.Log("Do"))
                .AddTo(this);
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;


namespace Samples.Section2.Observables
{
    /// <summary>
    /// OnErrorでObservalbeが破棄されることを確認
    /// </summary>
    public class ReleaseOnError : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            var subject = new Subject<string>();

            // Observableの購読
            subject
                .Select(str => int.Parse(str))
                .Subscribe(
                    x => Debug.Log(x),
                    ex => Debug.Log(ex.Message),
                    () => Debug.Log("OnCompleted()")
                );

            subject.OnNext("1");
            subject.OnNext("2");

            // int.Parse()で失敗して例外が発生する
            // そのため、ここでOnErrorが呼び出され、購読が完了する
            subject.OnNext("Three");
            subject.OnNext("4");


            subject.Subscribe(
                x => Debug.Log(x),
                () => Debug.Log("Completed")
            );

            // Subjectが直接例外発行したわけではないので、Subjectは元気に稼働中
            // Subjectは再利用可能
            subject.OnNext("Hello");
            subject.OnCompleted();
            subject.Dispose();
        }

    }
}

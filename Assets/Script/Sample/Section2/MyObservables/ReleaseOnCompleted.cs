using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace Samples.Section2.Observables
{
    /// <summary>
    /// OnCompletedでObservableが破棄されることを確認
    /// </summary>
    public class ReleaseOnCompleted : MonoBehaviour
    {
        // Start is called before the first frame update
        /// <summary>
        /// OnCompletedでObservableが破棄されることを確認
        /// </summary>
        void Start()
        {
            var subject = new Subject<int>();

            subject.Subscribe(
                x => Debug.Log(x),
                () => Debug.Log("OnCompleted")
            );

            subject.OnNext(1);
            subject.OnNext(2);
            subject.OnNext(3);

            subject.OnCompleted();

            subject.Dispose();
        }
    }
}

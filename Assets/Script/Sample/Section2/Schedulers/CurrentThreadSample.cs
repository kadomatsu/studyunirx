using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using System.Threading.Tasks;
using UniRx;


namespace Samples.Section2.Schedulers
{
    public class CurrentThreadSample : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            Debug.Log("Unity Main Thread ID:" + Thread.CurrentThread.ManagedThreadId);
        
            var subject = new Subject<Unit>();
            subject.AddTo(this);

            subject.ObserveOn(Scheduler.Immediate)
                .Subscribe(_ => 
                {
                    Debug.Log("Thread Id:" + Thread.CurrentThread.ManagedThreadId);
                });

            // メインスレッドにて、OnNext発行
            subject.OnNext(Unit.Default);

            // 別スレッドからOnNextを発行
            Task.Run(() => subject.OnNext(Unit.Default));

            // subject.OnCompleted();
        }
    }
}


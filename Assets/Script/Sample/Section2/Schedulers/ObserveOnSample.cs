using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Threading.Tasks;
using UniRx;

namespace Samples.Section2.Schedulers
{
    /// <summary>
    /// Schedulerを用いて
    /// メッセージ処理のタイミング変更をする
    /// </summary>
    public class ObserveOnSample : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            // ファイルをスレッドプール上で読み込む処理
            var task = Task.Run(() => File.ReadAllText(@"data.txt"));


            // タスクからObservableに変換
            // スレッドはスレッドプール上で変更なし
            task.ToObservable()
                .ObserveOn(Scheduler.MainThread) // ここでメインスレッドに切り替え
                .Subscribe( x => Debug.Log(x));
        }
    }
}


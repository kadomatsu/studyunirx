using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;
using System.Threading;

namespace Samples.Section2.Schedulers
{
    /// <summary>
    /// Schedulerのサンプル
    /// ファクトリメソッドと、Operatorの実行タイミング制御
    /// </summary>
    public class TimersSample : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            // MainThreadを指定
            // 「一秒経過直後に実行されたUpdate()」と同じタイミングに実行される
            Observable.Timer(TimeSpan.FromSeconds(1), Scheduler.MainThread)
                .Subscribe(x => Debug.Log("1秒経過しました"))
                .AddTo(this);

            // 未指定の場合は、MainThreadScheduler指定と同じ
            Observable.Timer(TimeSpan.FromSeconds(3), Scheduler.CurrentThread)
                .Subscribe()
                .AddTo(this);

            // MainThreadEndOfFrame指定のとき
            // 「一秒経過後直後のフレームのレンダリング後」に実行sられる
            Observable.Timer(TimeSpan.FromSeconds(1), Scheduler.MainThreadEndOfFrame)
                .Subscribe(X => Debug.Log("1秒経過しました"))
                .AddTo(this);

            // CurrentThread指定すると、そのまま同じスレッドで処理が実行される
            // このコードの場合は新しく作ったスレッド上でタイマのカウントを実行する
            new Thread(() =>
                {
                    Observable.Timer(TimeSpan.FromSeconds(3), Scheduler.MainThread)
                        .Subscribe()
                        .AddTo(this);

                    Observable.Timer(TimeSpan.FromSeconds(3), Scheduler.CurrentThread)
                        .Subscribe()
                        .AddTo(this);
                }
            ).Start();
        }
    }
}


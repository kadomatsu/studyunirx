using System;
using UnityEngine;

namespace Sample.Section2.MyObservers
{
    public class ObserveEventComponent : MonoBehaviour
    {
        // subject
        [SerializeField]
        private CountDownEventProvider m_countDownEventProvider;

        // Observerのインスタンス
        private PrintLogObserver<int> m_printLogObserver;
        private IDisposable m_disposable;


        // Start is called before the first frame update
        private void Start()
        {
            // observerのインスタンスを作成
            m_printLogObserver = new PrintLogObserver<int>();

            // IObservableのsubscribeを呼び出して、observerを登録する
            m_disposable = m_countDownEventProvider
                .CountDownObservable
                .Subscribe(m_printLogObserver);
        }

        private void OnDestroy()
        {
            // GameObject破棄時にイベント購読を中断する
            m_disposable?.Dispose();
        }
    }
}

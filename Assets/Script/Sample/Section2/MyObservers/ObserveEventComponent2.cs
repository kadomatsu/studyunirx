using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UniRx;

namespace Sample.Section2.MyObservers
{
    /// <summary>
    /// Subjectの糖衣構文でデリゲードを登録する
    /// </summary>
    public class ObserveEventComponent2 : MonoBehaviour
    {
        [SerializeField]
        private CountDownEventProvider m_countDownEventProvider;

        // Observerのインスタンス
        private PrintLogObserver<int> m_printLogObserver;

        private IDisposable m_disposable;
        private void Start()
        {
            // observerを登録する
            m_disposable = m_countDownEventProvider
                .CountDownObservable
                .Subscribe(
                    // printLogObserverに書いていた処理をデリゲードで登録可能
                    x => Debug.Log($"{x}"),
                    ex => Debug.Log(ex),
                    () => Debug.Log("OnCompled"));
        }
        private void OnDestroy()
        {
            m_disposable?.Dispose();
        }
    }
}
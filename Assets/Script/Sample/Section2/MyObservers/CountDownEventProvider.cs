using System;
using System.Collections;
using UniRx;
using UnityEngine;

namespace Sample.Section2.MyObservers
{
    /// <summary>
    /// PrintLogObserverに対して、Subjectを使ってイベントメッセージを送信する
    /// </summary>
    
    [Serializable]
    public class CountDownEventProvider : MonoBehaviour
    {
        // カウントする秒数
        [SerializeField] private int m_countSeconds = 10;

        // Subjectのインスタンス
        private Subject<int> m_subject;

        // SubjectのIObservableインターフェース部分のみ公開
        public IObservable<int> CountDownObservable => m_subject;

        private void Awake()
        {
            // subjectを生成する
            m_subject = new Subject<int>();
            
            // カウントダウンするコルーチンを起動する
            StartCoroutine(CountCoroutine());
        }

        /// <summary>
        /// 指定秒数カウントし、その都度メッセージを発行するコルーチン
        /// </summary>
        /// <returns></returns>
        private IEnumerator CountCoroutine()
        {
            var current = m_countSeconds;

            while (current > 0)
            {
                // 現在の値を発行する
                m_subject.OnNext(current);
                current--;
                yield return new WaitForSeconds(1.0f);
            }

            // 最後に0とOnCompletedメッセージを発行する
            m_subject.OnNext(0);
            m_subject.OnCompleted();
        }

        private void OnDestroy()
        {
            // gameObjectが破棄されたら、subjectも解散する
            m_subject.Dispose();
        }
    }
}

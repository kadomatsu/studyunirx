using System;
using UnityEngine;

namespace Sample.Section2.MyObservers
{
    public class PrintLogObserver<T> : IObserver<T>
    {
        public void OnNext(T value)
        {
            Debug.Log($"{value}");
        }
        public void OnError(Exception error)
        {
            Debug.Log(error);
        }
        public void OnCompleted()
        {
            Debug.Log("OnCompleted");
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;

namespace Others.Usagikun
{
    public class GameManager : MonoBehaviour
    {
        private Subject<Card> m_UseCardSubject;
        // カードをプレイしたときに通知する公開用(HOT)Observable
        public IObservable<Card> UseCardObservable {get; private set;}

        // Usagiのカード
        Card m_usagi = null;

        // Start is called before the first frame update
        void Awake()
        {
            m_UseCardSubject  = new Subject<Card>();
            UseCardObservable = m_UseCardSubject.AddTo(this);
            m_usagi = new Usagi();
        }
        void Start()
        {
            this.UseCard();
            this.UseCard();
            this.UseCard();
            this.UseCard();
            this.UseCard();
        }
        
        /// <summary>
        /// 何かしらのカードが使用されたとき呼ばれる関数
        /// </summary>
        void UseCard()
        {
            Debug.Log("UseCard");

            // 購読者に通知する
            m_UseCardSubject.OnNext(m_usagi);
        }
        
        private void OnDestroy()
        {
            m_UseCardSubject.Dispose();
        }
    }
}



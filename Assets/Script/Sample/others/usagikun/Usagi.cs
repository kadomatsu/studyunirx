using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Others.Usagikun;
using UniRx;

namespace Others.Usagikun
{

    public class Usagi : Card
    {
        // 死死死死死死死死死死死死死
        // ありえん気持ち悪い
        // 各カードごとに、GamaManagerを取得するのはキモくないかい
        // 目的はGameManager内で生成し（てしまった）たObserverなのだが
        // ここの分離よくわかっていないのでこうなってしまった
        private GameManager m_gameManager;

        public Usagi()
        {
            m_gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
            // 
            m_gameManager.UseCardObservable // カードが利用されたとき
                //.Where((x, num) => num == 2) // 2回
                .Subscribe(
                    x => Debug.Log("カードが２回プレイされたよー"),
                    ex => Debug.LogWarning(ex.Message),
                    () => Debug.Log("OnCompleted")
                );
        }
    }
}



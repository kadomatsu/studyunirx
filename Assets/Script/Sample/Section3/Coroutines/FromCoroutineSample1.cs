using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace Samples.Section3.Coroutine
{
    /// <summary>
    /// Observable.FromCoroutineでコルーチンをObservableへと変換d切る
    /// </summary>
    public class FromCoroutineSample1 : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            // コルーチンの終了をObservableで待ち受ける
            Observable.FromCoroutine(WaitingCoroutine, publishEveryYield: false)
                .Subscribe(
                    _ => Debug.Log("OnNext"),
                    () => Debug.Log("OnCompleted")
                )
                .AddTo(this);
        }

        // 3秒待つだけのコルーチン
        private IEnumerator WaitingCoroutine()
        {
            Debug.Log("Coroutine start");
            // 3sec 待機
            yield return new WaitForSeconds(3);

            Debug.Log("Coroutine finish");
        }

    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;

namespace Samples.Section3.Coroutine
{
    public class FromCoroutineSample3 : MonoBehaviour
    {
        // 長押し判定のしきい値
        private readonly float m_longPressThresholdSeconds = 3.0f;

        // Start is called before the first frame update
        void Start()
        {
            // 一定時間の長押しを検知するObservable
            Observable.FromCoroutine<bool>(observer => LongPushCoroutine(observer))
                .DistinctUntilChanged() // 連続した重複するメッセージを除去する
                .Subscribe(x => Debug.Log(x))
                .AddTo(this);
        }

        private IEnumerator LongPushCoroutine(IObserver<bool> observer)
        {
            var isPushed = false;
            var lastPushTime = Time.time;

            while(true)
            {
                if (Input.GetKey(KeyCode.Space))
                {
                    if (isPushed)
                    {
                        lastPushTime = Time.time;
                        isPushed = true;
                    }
                    else if (Time.time - lastPushTime > m_longPressThresholdSeconds)
                    {
                        // 一定時間以上押下され続けているなら、Trueを発行する
                        observer.OnNext(true);
                    }
                }
                else
                {
                    if (isPushed)
                    {
                        // 離されたらFalseを発行する
                        observer.OnNext(false);
                        isPushed = false;
                    }
                }
                yield return null;
            }
        }
    }
}

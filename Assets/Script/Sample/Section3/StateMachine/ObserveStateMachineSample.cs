using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

namespace Samples.Section3.StateMachine
{
    public class ObserveStateMachineSample : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            var animator = GetComponent<Animator>();
            
            var trigger = animator.GetBehaviour<ObservableStateMachineTrigger>();

            // AttackStateに遷移した通知
            var attackStart =
                trigger.OnStateEnterAsObservable() // Enter
                .Where(x => x.StateInfo.IsName("Attack"));

            // attakckから遷移した通知
            var attackEnd = trigger.OnStateExitAsObservable() // Exit
                .Where(x => x.StateInfo.IsName("Attack"));

            // AttackStateにいる間、毎フレーム処理を実行する
            this.UpdateAsObservable()
                .SkipUntil(attackStart) // スタートするまで、無視する
                .TakeUntil(attackEnd)   // 攻撃が終わるまで通知を取得
                .RepeatUntilDestroy(this)   // OnCompletedをしたら、はじめからやり直す
                .Subscribe(_ => { Debug.Log("Attack中");} )
                .AddTo(this);
        }
    }
}


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace Samples.Section3.Subjects
{
    /// <summary>
    /// BehaviorSubjectの挙動確認サンプル
    /// </summary>
    public class BehaviorSubjectSample : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            // BehaviorSubjectの定義には初期値が必要
            var behaviorSubject = new BehaviorSubject<int>(0);

            // メッセージ発行
            behaviorSubject.OnNext(1);

            // 購読する
            behaviorSubject.Subscribe(
                x => Debug.Log("OnNext:" + x),
                ex => Debug.Log("OnError" + ex),
                () => Debug.Log("OnCompleted")
            );

            // メッセージを発行
            behaviorSubject.OnNext(2);

            // valueを参照すると、最新の値を確認する
            Debug.Log("Last Value" + behaviorSubject.Value);

            behaviorSubject.OnNext(3);
            behaviorSubject.OnCompleted();

            behaviorSubject.Dispose();
        }
    }
}


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;

namespace Samples.Section3.Subjects.Async
{
    /// <summary>
    /// AsyncSubject 動作確認 サンプル
    /// </summary>
    public class AsyncSubjectSample : MonoBehaviour
    {
        private readonly AsyncSubject<Texture> m_playerTextureAsyncSubject
            = new AsyncSubject<Texture>();

        // プレイヤのテクスチャ情報
        public IObservable<Texture> PlayerTextureAsync => m_playerTextureAsyncSubject;

        private void Start()
        {
            // 起動時にテクスチャをロードする
            StartCoroutine(LoadTexture());
        }

        // テクスチャを読み込むコルーチン
        private IEnumerator LoadTexture()
        {
            var resource = Resources.LoadAsync<Texture>("Textures/player");

            yield return resource;

            // 読み込みが完了したら、AsyncSubjectで結果を通知する
            m_playerTextureAsyncSubject.OnNext(resource.asset as Texture);
            m_playerTextureAsyncSubject.OnCompleted();
        }
    }
}


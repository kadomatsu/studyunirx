using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace Samples.Section3.Subjects
{
    /// <summary>
    /// ReplaySubjectの挙動確認サンプル
    /// </summary>
    public class ReplaySubjectSample : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            // バッファサイズが3のReplaySubjectを生成する
            var subject = new ReplaySubject<int>(bufferSize: 3);

            // メッセージを発行する
            for (int i = 0; i < 10; i++ )
            {
                subject.OnNext(i);
            }

            // OnCompleted
            subject.OnCompleted();
            // subject.OnError(new System.Exception("Error"));

            // 購読する
            subject.Subscribe(
                x => Debug.Log("OnNext:" + x),
                ex => Debug.LogError("OnError:" + ex),
                () => Debug.Log("OnCompleted")
            );

            subject.Dispose();
        }
    }
}


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace Samples.Section3.Subjects
{
    /// <summary>
    /// SubjectでのObservableの作成サンプル
    /// </summary>
    public class SubjectSample : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            var subject = new Subject<int>();

            subject.OnNext(1);

            subject.Subscribe(
                x => Debug.Log(x),
                ex => Debug.Log(ex.Message),
                () => Debug.Log("Completed")
            );

            subject.OnNext(2);
            subject.OnNext(3);

            subject.OnCompleted();

            subject.Dispose();

        }
    }
}


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;


namespace Samples.Section3.Subjects.Async
{
    public class PlayerTextureChanger : MonoBehaviour
    {
        [SerializeField]
        private AsyncSubjectSample m_gameResourceProvider;
        // Start is called before the first frame update
        void Start()
        {
            // 
            m_gameResourceProvider.PlayerTextureAsync
                .Subscribe(SetMyTexture)
                .AddTo(this);
        }

        private void SetMyTexture(Texture newTexture)
        {
            var r = GetComponent<Renderer>();
            r.sharedMaterial.mainTexture =newTexture;
        }
    }
}




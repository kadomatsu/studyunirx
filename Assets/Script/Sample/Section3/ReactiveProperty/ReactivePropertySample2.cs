using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace Samples.Section3.ReactiveProperty
{
    /// <summary>
    /// 強制的にメッセージを発行するサンプル
    /// </summary>
    public class ReactivePropertySample2 : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            var health = new ReactiveProperty<int>(100);

            health.Subscribe(x => Debug.Log($"通知された値:{x}"));

            // 値が変わらないなら、通知は飛ばない
            Debug.Log("<valueを100に設定>");
            health.Value = 100;

            // 強制的に通知を飛ばすときは、SetValueAndForceNotifyを使って
            // 値を設定する
            Debug.Log("<valueの上書きを強制的に通知>");
            health.SetValueAndForceNotify(100);
            health.Dispose();
        }

    }
}


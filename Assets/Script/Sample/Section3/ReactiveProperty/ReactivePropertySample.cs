using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace Samples.Section3.ReactiveProperty
{
    public class ReactivePropertySample : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            // int型を扱うReactiveProperty
            var health = new ReactiveProperty<int>(100);

            // Valueにアクセスすれば現在設定されている値が読み取れる
            Debug.Log($"現在の値は: {health.Value}" );

            // ReactivePropertyを直接サブスクライブできる
            health.Subscribe(
                x => Debug.Log($"通知された値: {x}"),
                () => Debug.Log("Oncompleted")
            );

            // valueに値を設定すると、同時にOnNextが発行される
            health.Value = 50;

            Debug.Log("現在の値" + health.Value);

            // Dispose()するとOnCompletedメッセージを発行する
            health.Dispose();
        }
    }
}


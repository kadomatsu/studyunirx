namespace Samples.Section3.ReactiveProperty
{
    /// <summary>
    /// フルーツを表すEnum
    /// </summary>
    public enum Fruit
    {
        Apple,
        Banana,
        Peach,
        Melon,
        Orange
    }
}

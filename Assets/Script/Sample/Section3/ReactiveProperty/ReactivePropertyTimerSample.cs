using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace Samples.Section3.ReactiveProperty
{
    /// <summary>
    /// IReadOnlyReactivePropertyのサンプル
    /// </summary>
    class ReactivePropertyTimerSample : MonoBehaviour
    {
        // 実態としてReactivePropertyを定義する
        [SerializeField]
        private IntReactiveProperty m_current = new IntReactiveProperty(60);

        // 現在のタイマーの値
        // ReactiveProperty を IReadOnlyReactivePropertyにアップキャスト
        public IReadOnlyReactiveProperty<int> CurrentTime => m_current;

        private void Start()
        {
            StartCoroutine(CountDownCoroutine());
        }

        private IEnumerator CountDownCoroutine()
        {
            while (m_current.Value > 0)
            {
                // 1秒に1ずつ値を更新する
                m_current.Value--;
                yield return new WaitForSeconds(1);
            }
        }
    }
}



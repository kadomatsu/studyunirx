using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace Sample.Section3.ReactiveProperty
{
    /// <summary>
    /// subscribe時のメッセージをスキップする場合
    /// </summary>
    public class ReactivePropertySample3 : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            var health = new ReactiveProperty<int>(100);

            health
                // Subscribe()直後のOnNextメッセージを無視する
                .SkipLatestValueOnSubscribe()
                .Subscribe(x => Debug.Log("通知された値:" + x));
            
            health.Dispose();
        }
    }
}
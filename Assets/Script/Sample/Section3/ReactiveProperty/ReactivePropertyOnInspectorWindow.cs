using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace Samples.Section3.ReactiveProperty
{
    /// <summary>
    /// ReactiveProperty<T>の値をインスペクタウィンドウから設定する
    /// </summary>
    public class ReactivePropertyOnInspectorWindow : MonoBehaviour
    {
        // ジェネリック版 表示されない
        public ReactiveProperty<int> A;
        
        // Int固定版 表示される
        public IntReactiveProperty B;
    }
}


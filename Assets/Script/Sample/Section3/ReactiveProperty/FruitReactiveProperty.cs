using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;

namespace Samples.Section3.ReactiveProperty
{
    [Serializable]
    public class FruitReactiveProperty : ReactiveProperty<Fruit>
    {
        public FruitReactiveProperty()
        {
        }

        public FruitReactiveProperty(Fruit init) : base(init)
        {
        }
    }
}



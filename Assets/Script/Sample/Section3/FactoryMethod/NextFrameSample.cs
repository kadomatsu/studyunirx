using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace Samples.Section3.FactoryMethods
{
    public class NextFrameSample : MonoBehaviour
    {
        private Rigidbody m_regidbody;
        // Start is called before the first frame update
        void Start()
        {
            m_regidbody = GetComponent<Rigidbody>();
        }

        // Update is called once per frame
        void Update()
        {
            // スペースキーが押されたらRigidbodyに力を加える
            if (Input.GetKeyDown(KeyCode.Space))
            {
                // 次のFixedUpdateのタイミングで処理を実行する
                Observable.NextFrame(FrameCountType.FixedUpdate)
                    .Subscribe(_ => 
                    {
                        m_regidbody.AddForce(Vector3.up * 5, ForceMode.VelocityChange);
                    });
            }
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace Samples.Section3.UnityEvents
{
    public class EveryUpdateSample : MonoBehaviour
    {
        
        // Update is called once per frame
        void Start()
        {
            // 毎フレームオブジェクトの座標を更新する
            Observable.EveryUpdate()
                .Subscribe(_ =>
                    {
                        transform.position += Vector3.forward * Time.deltaTime;
                    }
                )
                .AddTo(this); // GameObjectが破棄された確実に止める
        }
    }
}



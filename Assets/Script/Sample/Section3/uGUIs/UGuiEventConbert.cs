using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UnityEngine.UI;


namespace Samples.Section3.uGUIs

{
    public class UGuiEventConbert : MonoBehaviour
    {
        [SerializeField]
        private Toggle uiToggle; // uGUIのトグルコンポーネント

        // Start is called before the first frame update
        void Start()
        {
            uiToggle.isOn = false;
            uiToggle.onValueChanged.AsObservable()
                .Subscribe(x => Debug.Log("現在の状態(AsObservable):" + x));

            // 各UI コンポーネントに用意された拡張メソッドを呼び出すパターン
            // Subscribeした瞬間に自動的に初期値が発行される
            uiToggle.OnValueChangedAsObservable()
                .Subscribe(x => Debug.Log("現在の状態(拡張メソッド):" + x));
            
            Debug.Log("---");

            uiToggle.isOn = true;
        }
    }
}


